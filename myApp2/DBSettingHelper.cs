﻿using System;
using Android.Content;
using Android.Database;
using Android.Database.Sqlite;

namespace myApp2
{
	public class DBSettingHelper : MainActivity
	{
		DBOpenHelper1 dbOpenHelper;
		public DBSettingHelper(Context context)
		{
			this.dbOpenHelper = new DBOpenHelper1(context);
		}
		//method for initializing the setting table in database
		public void addDefaultSetting(SettingClass setting)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;
			ContentValues cv = new ContentValues();
			cv.Put("pertime", setting.getPertime());
			cv.Put("frequency", setting.getFrequency());
			cv.Put("alarm", setting.getAlarm());
			/* insert data */
			db.Insert("setting", null, cv);
			db.Close();
		}
		// save setting to database
		public void save(SettingClass setting)
		{
			SQLiteDatabase db = dbOpenHelper.WritableDatabase;
			ContentValues cv = new ContentValues();
			cv.Put("pertime", setting.getPertime());
			cv.Put("frequency", setting.getFrequency());
			cv.Put("alarm", setting.getAlarm());
			// insert data
			db.Update("setting", cv, "settingID" + " = 1 ", null);
			db.Close();
		}

		/**
		 * @param
		 */

		//this can be used for searching, but the code need esome change so it can return many events with same key words
		public SettingClass getSetting()
		{
			SQLiteDatabase db = dbOpenHelper.ReadableDatabase;
			ICursor cursor = db.RawQuery("select * from setting where settingID=?", new String[] { "1" });
			if (cursor.MoveToFirst())
			{
				int uid2 = cursor.GetInt(cursor.GetColumnIndex("settingID"));
				String pertime = cursor.GetString(cursor.GetColumnIndex("pertime"));
				String frequency = cursor.GetString(cursor.GetColumnIndex("frequency"));
				String alarm = cursor.GetString(cursor.GetColumnIndex("alarm"));
				SettingClass setting = new SettingClass();
				setting.setID(uid2);
				setting.setPertime(pertime);
				setting.setFrequency(frequency);
				setting.setAlarm(alarm);
				return setting;
			}
			cursor.Close();
			return null;
		}

		public long getCount()
		{ //how much event there currently are
			SQLiteDatabase db = dbOpenHelper.ReadableDatabase;
			ICursor cursor = db.RawQuery("select count(*) from setting", null);
			cursor.MoveToFirst();
			long reslut = cursor.GetLong(0);
			return reslut; //return the number
		}

	}
}

