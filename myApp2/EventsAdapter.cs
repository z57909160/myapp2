﻿using System;
using Android.Content;
using Android.Provider;
using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using System.Linq;
using Android.App;
using Android.Content.Res;

namespace myApp2
{
	public class EventsAdapter : BaseAdapter
	{
		int theme;
		Context context;
		Activity activity;
		List<String[]> values;

		public override int Count
		{
			get { return values.Count; }
		}
		public override Java.Lang.Object GetItem(int position)
		{
			// could wrap a Contact in a Java.Lang.Object
			// to return it here if needed
			return null;
		}

		public override long GetItemId(int position)
		{
			return long.Parse(values.ElementAt(position)[0]);
		}
		public EventsAdapter(Activity a, List<String[]> values, int theme)
		{
			this.activity = a;
			this.values = values;
			this.theme = theme;
		}


		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			
			//LayoutInflater inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService); // Create a layout inflater
			//View rowView = inflater.Inflate(Resource.Layout.list_event, parent, false); // Create a view for the rows in the ListView

			var view = convertView ?? activity.LayoutInflater.Inflate(
				Resource.Layout.list_event, parent, false);
			ImageView eventCategory = (ImageView)view.FindViewById(Resource.Id.event_category);
			var eventName = view.FindViewById<TextView>(Resource.Id.event_name);
			var eventTime = view.FindViewById<TextView>(Resource.Id.event_time);
			var eventDate = view.FindViewById<TextView>(Resource.Id.event_date);
			var v = view.FindViewById<View>(Resource.Id.border_bottom);

			//******category icon
			if (values.ElementAt(position)[6].Equals( "0"))
			{
				eventCategory.SetImageResource(Resource.Drawable.general);
			}
			else if (values.ElementAt(position)[6].Equals ("1"))
			{
				eventCategory.SetImageResource(Resource.Drawable.holiday);
			}
			else if (values.ElementAt(position)[6].Equals ("2"))
			{
				eventCategory.SetImageResource(Resource.Drawable.birthday);
			}
			else if (values.ElementAt(position)[6].Equals ("3"))
			{
				eventCategory.SetImageResource(Resource.Drawable.assignment);
			}
			else {
				eventCategory.SetImageResource(Resource.Drawable.general);
			}

			//******border adapted to theme
			if(theme == 0)v.SetBackgroundColor(activity.Resources.GetColor(Resource.Color.main_color));
			else if (theme == 1) v.SetBackgroundColor(activity.Resources.GetColor(Resource.Color.natural_main_color));
			else if (theme == 2) v.SetBackgroundColor(activity.Resources.GetColor(Resource.Color.dark_main_color));
			else v.SetBackgroundColor(activity.Resources.GetColor(activity.Resources.GetColor(Resource.Color.main_color)));



			// Initialize and connect with the UI widgets
			//ImageView eventCategory = (ImageView)rowView.FindViewById(Resource.Id.event_category);


			// Set the content of UI widgets
			/*
			if (values.ElementAt(position)[6].Equals "0")
			{
				eventCategory.setImageResource(Resource.drawable.general);
			}
			else if (values.ElementAt(position)[6].Equals "1")
			{
				eventCategory.setImageResource(Resource.drawable.holiday);
			}
			else if (values.ElementAt(position)[6].Equals "2")
			{
				eventCategory.setImageResource(Resource.drawable.birthday);
			}
			else if (values.ElementAt(position)[6].Equals "3")
			{
				eventCategory.setImageResource(Resource.drawable.assignment);
			}
			else {
				eventCategory.setImageResource(Resource.drawable.general);
			}
			*/

			eventName.Text = values.ElementAt(position)[1];
			eventTime.Text = values.ElementAt(position)[3] + ", ";
			eventDate.Text = values.ElementAt(position)[4];

			return view; // Return the rowView
		}
	}
}

