﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;


namespace myApp2
{
	public class ReferenceAdapter : BaseAdapter
	{

		Activity activity;
		private List<string[]> values;
		public override int Count
		{
			get { return values.Count; }
		}
		public override long GetItemId(int position)
		{
			return position;
		}
		public ReferenceAdapter(Activity activity, List<string[]> values) 
		{
			this.activity = activity;
			this.values = values;
		}
		public override Java.Lang.Object GetItem(int position)
		{
			// could wrap a Contact in a Java.Lang.Object
			// to return it here if needed
			return null;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? activity.LayoutInflater.Inflate(
				Resource.Layout.list_reference, parent, false);
			// Initialize and connect with the UI widgets
			TextView fileName = (TextView)view.FindViewById(Resource.Id.file_name);
			TextView attribution = (TextView)view.FindViewById(Resource.Id.attribution_type);
			TextView authorName = (TextView)view.FindViewById(Resource.Id.author_name);



			// Set the content of UI widgets
			fileName.Text = values.ElementAt(position)[0];
			attribution.Text = values.ElementAt(position)[1]+", ";
			authorName.Text = values.ElementAt(position)[2];

			return view; // Return the rowView
		}
	}
}


