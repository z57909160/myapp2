﻿using System;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Support.V7.App;
using Java.Util;

namespace myApp2
{
	[Activity(Name = "com.companyname.myapp2.ClockActivity",
			  Label = "ClockActivity")]
	[IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
	public class ClockActivity : Activity
	{
		//initialize strings
		String AlarmName = null;
		String eventName = null;
		String eventTime = null;

		ISharedPreferences settings1; //get preference
		String vibrationStatus; //check if the vibration is turned on or off
		DBEventHelper dbEventHelper; //create helper for event
		EventClass e = new EventClass(); //initialize a new event
		DBSettingHelper dbSettingHelper;//initialize settingHelper
		SettingClass setting = new SettingClass();//initialize a new setting
		public static Java.Util.TimeZone tz = Java.Util.TimeZone.GetTimeZone("Australia/Sydney");

		private MediaPlayer mediaPlayer = new MediaPlayer(); //initialize mediaPlayer
		private Vibrator vibrator;//initialize vibrator

		protected override void OnCreate(Bundle savedInstanceState)
		{
			dbEventHelper = new DBEventHelper(this);
			dbSettingHelper = new DBSettingHelper(this);

			ISharedPreferences preferences = GetSharedPreferences(MainActivity.PREFS_NAME, 0);
			int theme = preferences.GetInt("theme", 0);
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.activity_clock);

			Bundle extras = Intent.Extras;
			//whether vibration is turned on or off
			settings1 = GetSharedPreferences("my_settings", 0);
			vibrationStatus = settings1.GetString("vibration", "on");
			eventName = extras.GetString("eventName");
			eventTime = extras.GetString("eventTime");

			//if it is an alarm or a controler
			if (extras == null)
			{
			}
			//if this is an event alarm
			else if (Intent.HasExtra("alarmID"))
			{
				if (vibrationStatus.Equals("on"))
				{//check if the vibration is on
					vibrator = (Vibrator)GetSystemService(Context.VibratorService);
					long[] pattern = { 100, 400, 100, 400 };   // off on off on
					vibrator.Vibrate(pattern, 0); // 0 means keep repeating until cancelation called
				}
				else {
					//do not vibrate
				}
				//check which alarm music is in use
				setting = dbSettingHelper.getSetting();
				AlarmName = setting.getAlarm();

				//play alarm music accordingly
				if (AlarmName.Equals("default_music"))
				{
					if (mediaPlayer.IsPlaying == true) { mediaPlayer.Stop(); }
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.default_music);
					mediaPlayer.Looping = true;
					mediaPlayer.Start();
				}
				else if (AlarmName.Equals("wake_up"))
				{
					if (mediaPlayer.IsPlaying == true) { mediaPlayer.Stop(); }
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.wake_up);
					mediaPlayer.Looping = true;
					mediaPlayer.Start();
				}
				else if (AlarmName.Equals("exciting"))
				{
					if (mediaPlayer.IsPlaying == true) { mediaPlayer.Stop(); }
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.exciting);
					mediaPlayer.Looping = true;
					mediaPlayer.Start();
				}
				else if (AlarmName.Equals("lyrical"))
				{
					if (mediaPlayer.IsPlaying == true) { mediaPlayer.Stop(); }
					mediaPlayer = MediaPlayer.Create(this, Resource.Raw.lyrical);
					mediaPlayer.Looping = true;
					mediaPlayer.Start();
				}

				createNotification();//create notification

				//build a dialog to remid user
				CustomDialog.Builder builder = new CustomDialog.Builder(this);
				//set dialog title and message
				builder.setMessage(eventName + "\n" + eventTime);
				builder.setTitle("eTime Reminder");
				builder.setPositiveButton("OK", delegate
				{

					mediaPlayer.Stop();
					if (vibrationStatus.Equals("on"))
					{
						vibrator.Cancel(); //stop vibrator
					}
					this.Finish();
				});
				builder.create(false, theme).Show();
			}


		}

		//create the notification when the event remind user
		public void createNotification()
		{
			String tittle = "Reminder";
			String subject = eventName;
			String body = eventTime;
			NotificationManager notif = (NotificationManager)GetSystemService(Context.NotificationService);
			Notification notify = new Notification(Resource.Drawable.ic_stat_name, tittle, Calendar.GetInstance(tz).TimeInMillis);
			PendingIntent pending = PendingIntent.GetActivity(Application.Context, 0, new Intent(), 0);

			notify.SetLatestEventInfo(Application.Context, subject, body, pending);
			notif.Notify(0, notify);

		}
	}
}

