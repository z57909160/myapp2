﻿using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;

namespace myApp2
{
	[Activity(MainLauncher = true)]
	public class CustomDialog : Dialog, IDialogInterfaceOnKeyListener
	{
		public CustomDialog(Context context) : base(context)
		{

		}

		public CustomDialog(Context context, int theme) : base(context, theme)
		{

		}

		public Boolean OnKey(IDialogInterface dialog, [GeneratedEnum] Keycode keyCode, KeyEvent e)
		{
			throw new NotImplementedException();
		}


		public class Builder
		{
			//initialize the id of widgets
			int id_title;
			int id_btn_p;
			int id_btn_n;
			int id_message;
			int id_content;
			int id_ck;
			View layout;

			private Context context;
			private String title;
			private IDialogInterfaceOnKeyListener mOnKeyListener;
			private String message;
			private IDialogInterfaceOnDismissListener mOnDismissListener;
			private Boolean mCancelable;
			private String positiveButtonText;
			private String negativeButtonText;
			private String checkboxText;
			private IDialogInterfaceOnCancelListener onCancelListener;
			private View contentView;
			private IDialogInterfaceOnClickListener positiveButtonClickListener;
			private IDialogInterfaceOnClickListener negativeButtonClickListener;


			private Action event_p;
			private Action event_n;
			CustomDialog dialog;

			public Builder(Context context)
			{

				this.context = context;
			}

			public Builder setMessage(String message)
			{
				this.message = message;
				return this;
			}

			/**
			 * Set the Dialog message from resource
			 *
			 * @param title
			 * @return
			 */
			public Builder setMessage(int message)
			{
				this.message = (String)context.GetText(message);
				return this;
			}

			/**
			 * Set the Dialog title from resource
			 *
			 * @param title
			 * @return
			 */
			public Builder setTitle(int title)
			{
				this.title = (String)context.GetText(title);
				return this;
			}

			/**
			 * Set the Dialog title from String
			 *
			 * @param title
			 * @return
			 */

			public Builder setTitle(String title)
			{
				this.title = title;
				return this;
			}

			public Builder setContentView(View v)
			{
				this.contentView = v;
				return this;
			}

			//check box
			public Builder setCheckBox(String text)
			{
				this.checkboxText = text;
				return this;
			}
			public Builder setCheckBox(int text)
			{
				this.positiveButtonText = (String)context
						.GetText(text);
				return this;
			}

			/**
			 * Set the positive button resource and it's listener
			 *
			 * @param positiveButtonText
			 * @return
			 */
			public Builder setPositiveButton(int positiveButtonText,
											 Action a)
			{
				this.positiveButtonText = positiveButtonText.ToString();
				this.event_p = a;
				return this;
			}


			public Builder setPositiveButton(String positiveButtonText,
											 Action a)
			{
				this.positiveButtonText = positiveButtonText.ToString();
				this.event_p = a;
				return this;
			}

			public Builder setNegativeButton(int negativeButtonText,
											  Action a)
			{
				this.negativeButtonText = negativeButtonText.ToString();
				this.event_n = a;
				return this;
			}
			public Builder setNegativeButton(String negativeButtonText,
											 Action a)
			{
				this.negativeButtonText = negativeButtonText;
				this.event_n = a;
				return this;

			}
			//return if the checkbox is selected
			public Boolean ifChecked()
			{
				CheckBox ck = (CheckBox)layout.FindViewById(id_ck);
				if (checkboxText != null && ck.Checked == true) return true;
				else return false;
			}



			//create dialog
			public CustomDialog create(Boolean onclick, int theme)
			{
				LayoutInflater inflater = (LayoutInflater)context
					.GetSystemService(Context.LayoutInflaterService);
				// instantiate the dialog with the custom Theme
				dialog = new CustomDialog(context, Resource.Style.Dialog);
				layout = inflater.Inflate(Resource.Layout.dialog_original, null);

				//check which theme is in use
				if (theme == 0)
				{ //original theme
					layout = inflater.Inflate(Resource.Layout.dialog_original, null);
					id_title = Resource.Id.title;
					id_btn_p = Resource.Id.positiveButton;
					id_btn_n = Resource.Id.negativeButton;
					id_message = Resource.Id.message;
					id_content = Resource.Id.content;
					id_ck = Resource.Id.checkBox0;
				}
				else if (theme == 1) //natural theme
				{
					layout = inflater.Inflate(Resource.Layout.dialog_natural, null);
					id_title = Resource.Id.title1;
					id_btn_p = Resource.Id.positiveButton1;
					id_btn_n = Resource.Id.negativeButton1;
					id_message = Resource.Id.message1;
					id_content = Resource.Id.content1;
					id_ck = Resource.Id.checkBox1;
				}
				else if (theme == 2) //dark theme
				{
					layout = inflater.Inflate(Resource.Layout.dialog_dark, null);
					id_title = Resource.Id.title2;
					id_btn_p = Resource.Id.positiveButton2;
					id_btn_n = Resource.Id.negativeButton2;
					id_message = Resource.Id.message2;
					id_content = Resource.Id.content2;
					id_ck = Resource.Id.checkBox2;

				}
				else {
					layout = inflater.Inflate(Resource.Layout.dialog_original, null);
					id_title = Resource.Id.title;
					id_btn_p = Resource.Id.positiveButton;
					id_btn_n = Resource.Id.negativeButton;
					id_message = Resource.Id.message;
					id_content = Resource.Id.content;
					id_ck = Resource.Id.checkBox0;
				}

				dialog.AddContentView(layout, new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent));
				// set the dialog title
				((TextView)layout.FindViewById(id_title)).Text = title;

				//set the checkbox
				if (checkboxText != null)
				{
					((CheckBox)layout.FindViewById(id_ck))
							.Text = checkboxText;
				}
				else {
					// if no confirm button just set the visibility to GONE
					layout.FindViewById(id_ck).Visibility = ViewStates.Gone;
				}
				// set the confirm button
				if (positiveButtonText != null)
				{

					((Button)layout.FindViewById(id_btn_p))
							.Text = positiveButtonText;
					if (event_p != null)
					{
						Button a = (Button)layout.FindViewById<Button>(id_btn_p);
						a.Click += delegate
						{
							event_p();
							dialog.Dismiss();//dismiss after clicked
						};

					}
				}
				else {
					// if no confirm button just set the visibility to GONE
					layout.FindViewById(id_btn_p).Visibility = ViewStates.Gone;
				}
				// set the cancel button
				if (negativeButtonText != null)
				{
					((Button)layout.FindViewById(id_btn_n))
						.Text = negativeButtonText;
					if (event_n != null)
					{
						Button b = (Button)layout.FindViewById<Button>(id_btn_n);

						b.Click += delegate { dialog.Dismiss(); };
					}
				}
				else {
					// if no confirm button just set the visibility to GONE
					layout.FindViewById(id_btn_n).Visibility = ViewStates.Gone;
				}
				// set the content message
				if (message != null)
				{
					((TextView)layout.FindViewById(id_message)).Text = message;
				}
				else if (contentView != null)
				{
					// if no message set
					// add the contentView to the dialog body
					((LinearLayout)layout.FindViewById(id_content))
						.RemoveAllViews();
					((LinearLayout)layout.FindViewById(id_content))
						.AddView(contentView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FillParent, ViewGroup.LayoutParams.FillParent));
				}
				dialog.SetContentView(layout);
				dialog.SetCancelable(onclick);
				return dialog;
			}
		}
	}
}

