﻿using System;
namespace myApp2
{
	public class EventClass
	{
		//properties
		int e_id;
		String e_name;
		String e_des;
		String e_sd;
		String e_st;
		int e_pretime;
		int e_frequency;
		Boolean e_status;
		String alarmID;
		int e_category_id;

		//set value to an event
		public void setID(int id) { this.e_id = id; }
		public void setName(String name) { this.e_name = name; }
		public void setDescription(String des) { this.e_des = des; }
		public void setStartDate(String sd) { this.e_sd = sd; }
		public void setStartTime(String st) { this.e_st = st; }
		public void setStatus(bool status) { this.e_status = status; }
		public void setAlarmID(string alarm_ID) { this.alarmID = alarm_ID; }
		public void setCategoryID(int categoryID) { this.e_category_id = categoryID; }
		public void setPretime(int pretime) { this.e_pretime = pretime; }
		public void setFrequency(int frequency) { this.e_frequency = frequency; }

		//get value from an event
		public int getID() { return this.e_id; }
		public String getName() { return this.e_name; }
		public String getDescription() { return this.e_des; }
		public String getStartDate() { return this.e_sd; }
		public String getStartTime() { return this.e_st; }
		// Note: guys, getStatus is int, not boolean, because sql doesn't have boolean
		public int getStatus()
		{
			if (this.e_status == false) return 0;
			else return 1;
		}
		public String getAlarmID()
		{
			return this.alarmID;
		}
		public int getCategoryID() { return this.e_category_id; }
		public int getPretime() { return this.e_pretime; }
		public int getFrequency() { return this.e_frequency; }
	}
}


