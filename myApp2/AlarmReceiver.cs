﻿
using System;
using Android.App;
using Android.Content;
using Android.OS;
using Java.Util;

namespace myApp2
{
	[BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
	public class AlarmReceiver : BroadcastReceiver
	{
		//initialize extras
		int count;
		string alarmID = null;
		string name = null;
		string time = null;
		long alarmStartTime;
		long eventTimeInMillis;
		long frequency;
		static Java.Util.TimeZone tz = Java.Util.TimeZone.GetTimeZone("Australia/Sydney");
		public override void OnReceive(Context context, Intent intent)
		{

			Intent i = new Intent(context, typeof(ClockActivity));
			//get value from extra
			count = intent.Extras.GetInt("count");
			name = intent.Extras.GetString("eventName");
			time = intent.Extras.GetString("eventTime");

			eventTimeInMillis = intent.Extras.GetLong("eventTimeInMillis");
			frequency = intent.Extras.GetLong("eventFrequency");
			alarmID = intent.Extras.GetString("alarmID");


			Calendar now = Calendar.GetInstance(tz);// get current time
										   //always be consistent with the phone time
			now.Set(CalendarField.Second, DateTime.Now.Second);
			now.Set(CalendarField.Minute, DateTime.Now.Minute);
			now.Set(CalendarField.HourOfDay, DateTime.Now.Hour);
			now.Set(CalendarField.DayOfMonth, DateTime.Now.Day);
			now.Set(CalendarField.Month, DateTime.Now.Month);
			now.Set(CalendarField.Year, DateTime.Now.Year);
			alarmStartTime = now.TimeInMillis;

			//put extra to pass to ClockAcitivity
			i.PutExtra("count", count);
			i.PutExtra("alarmID", alarmID);
			i.PutExtra("eventName", name);
			i.PutExtra("eventTime", time);

			//start ClockActivity
			i.AddFlags(ActivityFlags.NewTask);
			context.StartActivity(i);
			check(context);

		}
		//check if another alarm is needed by comparing the time
		public void check(Context ctxt)
		{
			long timeSpan;
			AlarmManager am1 = (AlarmManager)ctxt.GetSystemService(Activity.AlarmService);
			// if current time is before event time, then check how to set next alarm
			if (eventTimeInMillis > alarmStartTime)
			{

				if (eventTimeInMillis - alarmStartTime > frequency)
				{
					//if a frequency is still allowed for set another alarm
					//update
					count++;
					alarmStartTime = alarmStartTime + frequency;
					timeSpan = frequency;

				}
				//otherwise, set the alarm directly to the event time
				else {
					//update
					count++;
					timeSpan = eventTimeInMillis - alarmStartTime;
					alarmStartTime = eventTimeInMillis;
				}

				//preparing for another alarm
				Intent intent_Alarm = new Intent(ctxt, typeof(AlarmReceiver));
				intent_Alarm.PutExtra("alarmID", alarmID);
				intent_Alarm.PutExtra("eventName", name);
				intent_Alarm.PutExtra("eventTime", time);
				intent_Alarm.PutExtra("alarmStartTime", alarmStartTime.ToString());//long
				intent_Alarm.PutExtra("eventTimeInMillis", eventTimeInMillis.ToString()); //long
				intent_Alarm.PutExtra("eventFrequency", frequency.ToString()); //long
				intent_Alarm.PutExtra("count", count);

				PendingIntent pi = PendingIntent.GetBroadcast(ctxt, int.Parse(alarmID), intent_Alarm, PendingIntentFlags.UpdateCurrent);
				am1.SetExact(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime()+timeSpan, pi);
			}
			else {
				//do nothing
			}

		}


	}
}