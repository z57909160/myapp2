﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using Android.App;
using Android.Content;
using Android.Database;
using Android.Database.Sqlite;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Text;
using Java.Util;
using Android.Support;
using Android.Support.V7.Widget;
//Ambiguities
using Fragment = Android.App.Fragment;
using Android.Content.Res;
using Android.Support.V4.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Text;
using Java.Lang;
using Android.Support.V7.App;
using Android.Content.PM;

namespace myApp2
{
	[Activity(Label = "McClock", MainLauncher = true, Icon = "@mipmap/icon", ConfigurationChanges = ConfigChanges.Locale)]

	public class MainActivity : ActionBarActivity, DrawerAdapter.OnItemClickListener
	{

		private DrawerLayout mDrawerLayout;
		private RecyclerView mDrawerList;
		private Android.Support.V4.App.ActionBarDrawerToggle mDrawerToggle;
		public int navigationSection = 0;
		DateFormat dateFormat;
		private ICursor cursor;

		public List<string[]> myList;
		public List<string[]> upcomingList;
		public List<string[]> pastList;
		public List<string[]> generalList;
		public List<string[]> holidayList;
		public List<string[]> birthdayList;
		public List<string[]> assignmentList;
		public List<string[]> searchList;
		public ListView EventList;

		int theme;
		Locale locale;
		int language;
		string languageToLoad;

		private bool mSearchOpened;
		EditText searchBar;
		private IMenuItem mSearchAction;
		private Drawable mIconCloseSearch;
		private Drawable mIconOpenSearch;
		private string mSearchQuery;
		private ImageView titleView;

		private string mDrawerTitle;
		private string[] mPlanetTitles;

		public static string PREFS_NAME = "PreferencesFile";
		SQLiteDatabase db;
		DBOpenHelper1 dbOpenHelper;
		DBEventHelper dbEventHelper;
		DBSettingHelper dbSettingHelper;
		SettingClass setting;

		protected string[] items;
		static public Color a;

		private bool activityCreated = false;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			// Get a SharedPreferences and its values
			ISharedPreferences preferences = GetSharedPreferences(PREFS_NAME, 0);
			theme = preferences.GetInt("theme", 0);
			language = preferences.GetInt("language", 0);

			// Set the style of the theme depending on the value of variable theme
			if (theme == 0)
			{
				SetTheme(Resource.Style.AppTheme);
			}
			else if (theme == 1)
			{
				SetTheme(Resource.Style.NaturalAppTheme);
			}
			else if (theme == 2)
			{
				SetTheme(Resource.Style.DarkAppTheme);
			}
			else {
				SetTheme(Resource.Style.AppTheme);
			}

			// Set the language for the activity
			if (language == 0)
			{
				languageToLoad = "en";

			}
			else if (language == 1)
			{
				languageToLoad = "zh";
			}
			else if (language == 2)
			{
				languageToLoad = "vi";
			}
			else if (language == 3)
			{
				languageToLoad = "in";
			}
			else {
				languageToLoad = "en";
			}
			locale = new Locale(languageToLoad);
			Locale.Default = locale;
			Configuration config = new Configuration();
			config.Locale = locale;
			BaseContext.Resources.UpdateConfiguration(config,
					BaseContext.Resources.DisplayMetrics);


			//*********************************************************
			a = this.Resources.GetColor(Resource.Color.main_color);

			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Main);

			activityCreated = true;

			EventList = (ListView)FindViewById<ListView>(Resource.Id.listView);
			titleView = (ImageView)FindViewById<ImageView>(Resource.Id.pageTopIcon);
			//Add Button
			ImageButton addButton = (ImageButton)FindViewById<ImageButton>(Resource.Id.add_button);

			// Set our view from the "main" layout resource
			// Set the theme to user's choice or default
			if (theme == 0)
			{
				titleView.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_main));
				titleView.SetPadding(0, 5, 0, 0);
				//titleView.LayoutParameters = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent,ViewGroup.LayoutParams.WrapContent);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				addButton.SetBackgroundDrawable(GetDrawable(Resource.Drawable.oval));
				Window.DecorView.SetBackgroundColor(Color.White);
			}
			else if (theme == 1)
			{
				titleView.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_natural));
				titleView.SetPadding(0, 5, 0, 0);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.natural_main_color)));
				addButton.SetBackgroundDrawable(GetDrawable(Resource.Drawable.naturaloval));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.natural_background_color));
			}
			else if (theme == 2)
			{
				titleView.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_dark));
				titleView.SetPadding(0, 5, 0, 0);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.dark_main_color)));
				addButton.SetBackgroundDrawable(GetDrawable(Resource.Drawable.darkoval));
				EventList.SetBackgroundColor(Resources.GetColor(Resource.Color.dark_background_color));
				Window.DecorView.SetBackgroundColor(Resources.GetColor(Resource.Color.dark_background_color));

			}
			else {
				titleView.SetBackgroundDrawable(GetDrawable(Resource.Drawable.icon2_main));
				titleView.SetPadding(0, 5, 0, 0);
				SupportActionBar.SetBackgroundDrawable(new ColorDrawable(Resources.GetColor(Resource.Color.main_color)));
				addButton.SetBackgroundDrawable(GetDrawable(Resource.Drawable.oval));
				Window.DecorView.SetBackgroundColor(Color.White);
			}
			myList = new List<string[]>();
			pastList = new List<string[]>();
			upcomingList = new List<string[]>();
			generalList = new List<string[]>();
			holidayList = new List<string[]>();
			birthdayList = new List<string[]>();
			assignmentList = new List<string[]>();
			searchList = new List<string[]>();
			dbOpenHelper = new DBOpenHelper1(this);
			dbEventHelper = new DBEventHelper(this);




			addButton.Click += delegate
			{
				var addActivityIntent = new Intent(this, typeof(AddActivity));
				StartActivity(addActivityIntent);
			};


			//*******************************************************************************
			//*******************************************************************************
			mDrawerTitle = this.Title;
			mPlanetTitles = this.Resources.GetTextArray(Resource.Array.event_array);
			mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			mDrawerList = FindViewById<RecyclerView>(Resource.Id.left_drawer);

			// set a custom shadow that overlays the main content when the drawer opens
			mDrawerLayout.SetDrawerShadow(Resource.Drawable.drawer_shadow, GravityCompat.Start);
			// improve performance by indicating the list if fixed size.
			mDrawerList.HasFixedSize = true;
			mDrawerList.SetLayoutManager(new LinearLayoutManager(this));


			// set up the drawer's list view with items and click listener
			mDrawerList.SetAdapter(new DrawerAdapter(mPlanetTitles, this));

			// enable ActionBar app icon to behave as action to toggle nav drawer

			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetHomeButtonEnabled(true);

			// ActionBarDrawerToggle ties together the the proper interactions
			// between the sliding drawer and the action bar app icon

			mDrawerToggle = new MyActionBarDrawerToggle(this, mDrawerLayout,
				Resource.Drawable.ic_drawer,
				Resource.String.navigation_drawer_open,
				Resource.String.navigation_drawer_close);

			mDrawerLayout.SetDrawerListener(mDrawerToggle);

			//initialise the setting page
			//it's possible user will go directly to add page
			//so have to initialise at the very start of using this app
			//so have to initialise at the very start of using this app
			dbSettingHelper = new DBSettingHelper(this);
			setting = new SettingClass();
			if (dbSettingHelper.getCount() == 0)  //if it's a first-time run
			{
				setting.setPertime("3 days");
				setting.setFrequency("1 day");
				setting.setAlarm("default_music");
				dbSettingHelper.addDefaultSetting(setting);
			}

			// Getting the icons.
			mIconOpenSearch = Resources.GetDrawable(Resource.Drawable.search);
			mIconCloseSearch = Resources.GetDrawable(Resource.Drawable.close);
			if (mSearchOpened)
			{
				openSearchBar(mSearchQuery);
			}

			cursor = dbEventHelper.getCursor(); // get the cursor for reading database purposes.
												// Check if cursor is empty or filled. If filled, it will query table, and if empty, it will create a toast.
			if (cursor != null && cursor.Count > 0)
			{

				queryTable(cursor);

			}
			else {
				Toast.MakeText(Application.Context, GetText(Resource.String.noevents_text), ToastLength.Short).Show();
			}

			if (savedInstanceState == null) //first launch
				selectItem(0);

			//******************************************

			//*********************************************

			EventList.ItemClick += (object sender, AdapterView.ItemClickEventArgs args) => OnListItemClick(sender, args);

			List<string[]> aaaa = new List<string[]>();

		}

		void queryTable(ICursor cursor1)
		{
			myList.Clear();
			while (cursor.MoveToNext())
			{
				int id = cursor.GetInt(cursor.GetColumnIndex("_id"));
				string name = cursor.GetString(cursor.GetColumnIndex("name"));
				string desc = cursor.GetString(cursor.GetColumnIndex("description"));
				string startTime = cursor.GetString(cursor.GetColumnIndex("starttime"));
				string startDate = cursor.GetString(cursor.GetColumnIndex("startdate"));
				int upOrPast = cursor.GetInt(cursor.GetColumnIndex("uporpast"));
				int categoryID = cursor.GetInt(cursor.GetColumnIndex("categoryID"));
				//string alarmID = cursor.GetString(cursor.GetColumnIndex("alarmID"));
				dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				Date date = new Date();
				string stringDateData = startDate + " " + startTime;
				// Check if event is pass or upcoming, and modify it on database.
				try
				{

					Date dateData = dateFormat.Parse(stringDateData);

					if (dateData.CompareTo(date) > 0)
					{
						dbEventHelper.modifyEvent(id, name, desc, startDate, startTime, 1, categoryID);
					}
					else
					{
						dbEventHelper.modifyEvent(id, name, desc, startDate, startTime, 0, categoryID);
					}
					upOrPast = cursor.GetInt(cursor.GetColumnIndex("uporpast"));
				}
				catch (ParseException e)
				{
					e.PrintStackTrace();
				}
				string[] mystringArray = new string[7]; // Create a string Variable with a size of 3

				mystringArray[0] = id.ToString(); // Set the first element of mystringArray with value of id in string
				mystringArray[1] = name; // Set the second element of mystringArray with the value of singer
				mystringArray[2] = desc; // Set the third value of mystringArray with the value of song
				mystringArray[3] = startTime;
				mystringArray[4] = startDate;
				mystringArray[5] = upOrPast.ToString();
				mystringArray[6] = categoryID.ToString();

				myList.Add(mystringArray); // add mystringArray to myList
			}
		}


		protected void OnListItemClick(object sender, EventArgs e)
		{
			AdapterView.ItemClickEventArgs arg = (AdapterView.ItemClickEventArgs)e;
			string[] rowview = myList[arg.Position];

			//take the value in the row of list
			string evid = rowview[0];
			Intent intent =
				new Intent(this, typeof(DetailsActivity));
			intent.PutExtra("eventid", evid);

			StartActivity(intent);
		}

		// Filter myList array to only events that are upcoming
		public void getUpcomingTable()
		{
			int i = 0;
			upcomingList = new List<string[]>();

			for (i = 0; i < myList.Count; i++)
			{
				string[] tempArray = new string[7];
				tempArray = myList.ElementAt(i);

				if (tempArray[5] == "1")
				{
					upcomingList.Add(tempArray);
				}
			}

		}

		// Filter myList array to only events that are past
		void getPastTable()
		{
			pastList = new List<string[]>();
			int i;

			for (i = 0; i < myList.Count; i++)
			{
				string[] tempArray = new string[7];
				tempArray = myList.ElementAt(i);

				if (tempArray[5] == "0")
				{
					pastList.Add(tempArray);
				}
			}
		}

		// Filter myList array to only upcoming events that category is general
		void getGeneralTable()
		{
			generalList = new List<string[]>();
			int i;
			for (i = 0; i < myList.Count; i++)
			{
				string[] tempArray = new string[7];
				tempArray = myList.ElementAt(i);
				if (tempArray[6] == "0" && tempArray[5] == "1")
				{
					generalList.Add(tempArray);
				}
			}
		}

		// Filter myList array to only upcoming events that category is holiday
		void getHolidayTable()
		{
			holidayList = new List<string[]>();
			int i;

			for (i = 0; i < myList.Count; i++)
			{
				string[] tempArray = new string[7];
				tempArray = myList.ElementAt(i);

				if (tempArray[6] == "1" && tempArray[5] == "1")
				{
					holidayList.Add(tempArray);
				}
			}
		}

		// Filter myList array to only upcoming events that category is birthday
		void getBirthdayTable()
		{
			birthdayList = new List<string[]>();
			int i;

			for (i = 0; i < myList.Count; i++)
			{
				string[] tempArray = new string[7];
				tempArray = myList.ElementAt(i);

				if (tempArray[6] == "2" && tempArray[5] == "1")
				{
					birthdayList.Add(tempArray);
				}
			}
		}

		// Filter myList array to only upcoming events that category is assignment
		void getAssignmentTable()
		{
			assignmentList = new List<string[]>();
			int i;

			for (i = 0; i < myList.Count; i++)
			{
				string[] tempArray = new string[7];
				tempArray = myList.ElementAt(i);

				if (tempArray[6] == "3" && tempArray[5] == "1")
				{
					assignmentList.Add(tempArray);
				}
			}
		}

		// Sorts the list in order of from past closest date to farthest date
		List<string[]> sortDescendingList(List<string[]> list)
		{
			bool flag = true;
			string[] temp;
			Date dateOne;
			Date dateTwo;

			while (flag)
			{
				flag = false;
				for (int i = 0; i < list.Count - 1; i++)
				{
					string[] arrayOne = list.ElementAt(i);
					string[] arrayTwo = list.ElementAt(i + 1);

					string stringDateOne = arrayOne[4] + " " + arrayOne[3];
					string stringDateTwo = arrayTwo[4] + " " + arrayTwo[3];

					try
					{
						dateOne = dateFormat.Parse(stringDateOne);
						dateTwo = dateFormat.Parse(stringDateTwo);
						if (dateOne.CompareTo(dateTwo) <= 0)
						{
							temp = list.ElementAt(i);
							list[i] = list[i + 1];
							list[i + 1] = temp;
							flag = true;
						}

					}
					catch (ParseException e)
					{
						e.PrintStackTrace();
					}

				}
			}

			return list;
		}
		// Sorts the list in order of from future closest date to farthest date
		List<string[]> sortAscendingList(List<string[]> list)
		{
			bool flag = true;
			string[] temp;
			Date dateOne;
			Date dateTwo;

			while (flag)
			{
				flag = false;
				for (int i = 0; i < list.Count - 1; i++)
				{
					string[] arrayOne = list.ElementAt(i);
					string[] arrayTwo = list.ElementAt(i + 1);

					string stringDateOne = arrayOne[4] + " " + arrayOne[3];
					string stringDateTwo = arrayTwo[4] + " " + arrayTwo[3];

					try
					{
						dateOne = dateFormat.Parse(stringDateOne);
						dateTwo = dateFormat.Parse(stringDateTwo);
						if (dateOne.CompareTo(dateTwo) > 0)
						{
							temp = list.ElementAt(i);
							list[i] = list[i + 1];
							list[i + 1] = temp;
							flag = true;
						}

					}
					catch (ParseException e)
					{
						e.PrintStackTrace();
					}

				}
			}
			myList = list;
			return list;
		}


		void OnButtonClicked(object sender, EventArgs e)
		{

			db = dbOpenHelper.WritableDatabase;
			//put content value
			ContentValues cv = new ContentValues();
			cv.Put("pretime", "pretime1");
			cv.Put("frequency", "frequency1");
			cv.Put("alarm", "alarm1");
			/* insert data */
			db.Insert("setting", null, cv);
			db.Close();
		}


		//****************************************************************
		//****************************************************************
		internal class MyActionBarDrawerToggle : Android.Support.V4.App.ActionBarDrawerToggle
		{
			MainActivity owner;

			public MyActionBarDrawerToggle(MainActivity activity, DrawerLayout layout, int imgRes, int openRes, int closeRes)
				: base(activity, layout, imgRes, openRes, closeRes)
			{
				owner = activity;
			}

			public override void OnDrawerClosed(View drawerView)
			{
				owner.SupportActionBar.Title = owner.Title;
				owner.InvalidateOptionsMenu();
			}

			public override void OnDrawerOpened(View drawerView)
			{
				owner.SupportActionBar.Title = owner.mDrawerTitle;
				owner.mDrawerLayout.DrawingCacheBackgroundColor = a;
				owner.InvalidateOptionsMenu();
			}
		}

		public override bool OnCreateOptionsMenu(IMenu menu)
		{

			menu.Add(0, Resource.Id.action_search, 0, "androidDemo").SetIcon(Resource.Drawable.search).SetShowAsAction(ShowAsAction.Always);
			// Inflate the menu; this adds items to the action bar if it is present.
			this.MenuInflater.Inflate(Resource.Menu.navigation_drawer, menu);
			return true;
		}

		/* Called whenever we call invalidateOptionsMenu() */
		public override bool OnPrepareOptionsMenu(IMenu menu)
		{
			mSearchAction = menu.FindItem(Resource.Id.action_search);
			// If the nav drawer is open, hide action items related to the content view
			bool drawerOpen = mDrawerLayout.IsDrawerOpen(mDrawerList);
			menu.FindItem(Resource.Id.action_search).SetVisible(!drawerOpen);
			return base.OnPrepareOptionsMenu(menu);
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			// The action bar home/up action should open or close the drawer.
			// ActionBarDrawerToggle will take care of this.
			if (mDrawerToggle.OnOptionsItemSelected(item))
			{
				return true;
			}
			// Handle action buttons
			switch (item.ItemId)
			{
				case Resource.Id.action_search:
					if (mSearchOpened)
					{
						closeSearchBar();
					}
					else {
						openSearchBar(mSearchQuery);
					}
					return true;

				case Resource.Id.action_settings:
					{
						var settingActivityIntent = new Intent(this, typeof(SettingActivity));
						StartActivity(settingActivityIntent);

						return true;
					}

				case Resource.Id.action_about:
					{
						var aboutActivityIntent = new Intent(this, typeof(AboutActivity));
						StartActivity(aboutActivityIntent);
						return true;
					}
				default:
					return base.OnOptionsItemSelected(item);
			}
		}

		/* The click listener for RecyclerView in the navigation drawer */
		public void OnClick(View view, int position)
		{
			navigationSection = position;
			cursor = dbEventHelper.getCursor(); // get the cursor for reading database purposes.
												// Check if cursor is empty or filled. If filled, it will query table, and if empty, it will create a toast.
			if (cursor != null && cursor.Count > 0)
			{

				queryTable(cursor);
			}
			else {
				Toast.MakeText(Application.Context, GetText(Resource.String.noevents_text), ToastLength.Short).Show();
			}

			selectItem(position);


		}

		private void selectItem(int position)
		{
			navigationSection = position;
			switch (navigationSection)
			{
				case 0:
					getUpcomingTable();
					upcomingList = sortAscendingList(upcomingList);
					if (upcomingList.Count() > 0)
					{
						EventsAdapter adapter = new EventsAdapter(this, upcomingList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter; // Set the adapter for listView with adapter
					}

					break;
				case 1:
					getPastTable();
					pastList = sortDescendingList(pastList);
					if (pastList != null)
					{

						EventsAdapter adapter = new EventsAdapter(this, pastList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter;  // Set the adapter for listView with adapter
					}

					break;
				case 2:
					getGeneralTable();
					generalList = sortAscendingList(generalList);
					if (generalList != null)
					{

						EventsAdapter adapter = new EventsAdapter(this, generalList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter; // Set the adapter for listView with adapter
					}

					break;
				case 3:
					getHolidayTable();
					holidayList = sortAscendingList(holidayList);
					if (holidayList != null)
					{
						EventsAdapter adapter = new EventsAdapter(this, holidayList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter; // Set the adapter for listView with adapter
					}

					break;
				case 4:
					getBirthdayTable();
					birthdayList = sortAscendingList(birthdayList);
					if (birthdayList != null)
					{

						EventsAdapter adapter = new EventsAdapter(this, birthdayList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter;  // Set the adapter for listView with adapter
					}

					break;
				case 5:
					getAssignmentTable();
					assignmentList = sortAscendingList(assignmentList);
					if (assignmentList != null)
					{

						EventsAdapter adapter = new EventsAdapter(this, assignmentList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter;  // Set the adapter for listView with adapter
					}

					break;
				default:
					if (myList != null)
					{
						myList = sortDescendingList(myList);
						EventsAdapter adapter = new EventsAdapter(this, myList, theme); // Create an adapter variable and initialize it.
						EventList.Adapter = adapter; // Set the adapter for listView with adapter
					}
					break;
			}

			//pf = new CustomFragment(position,abc);
			// update the main content by replacing fragments
			var fragment = CustomFragment.NewInstance(position);
			var fragmentManager = this.FragmentManager;
			var ft = fragmentManager.BeginTransaction();
			ft.Replace(Resource.Id.content_frame, fragment);
			ft.Commit();
			// update selected item title, then close the drawer
			Title = mPlanetTitles[position];
			mDrawerLayout.CloseDrawer(mDrawerList);
		}

		//		private void SetTitle (string title)
		//		{
		//			this.Title = title;
		//			SupportActionBar.Title = title;
		//		}

		protected override void OnTitleChanged(Java.Lang.ICharSequence title, Android.Graphics.Color color)
		{
			//base.OnTitleChanged (title, color);
			SupportActionBar.Title = title.ToString();
		}

		/**
	     * When using the ActionBarDrawerToggle, you must call it during
	     * onPostCreate() and onConfigurationChanged()...
	     */

		protected override void OnPostCreate(Bundle savedInstanceState)
		{
			base.OnPostCreate(savedInstanceState);
			// Sync the toggle state after onRestoreInstanceState has occurred.
			mDrawerToggle.SyncState();
		}

		public override void OnConfigurationChanged(Configuration newConfig)
		{
			base.OnConfigurationChanged(newConfig);
			// Pass any configuration change to the drawer toggls
			mDrawerToggle.OnConfigurationChanged(newConfig);

		}

		// Method when search bar is closed
		private void closeSearchBar()
		{
			// Remove custom view
			SupportActionBar.SetDisplayShowCustomEnabled(false);

			// Change search icon
			// Reference: http://blog.lovelyhq.com/implementing-a-live-list-search-in-android-action-bar/
			mSearchAction.SetIcon(mIconOpenSearch);
			mSearchOpened = false;

			EventsAdapter adapter;

			// Set the adapter back to the previous list view before search
			switch (navigationSection)
			{
				case 0:
					adapter = new EventsAdapter(this, upcomingList, theme);

					EventList.Adapter = adapter;
					break;
				case 1:
					adapter = new EventsAdapter(this, pastList, theme);
					EventList.Adapter = adapter;
					break;
				case 2:
					adapter = new EventsAdapter(this, generalList, theme);
					EventList.Adapter = adapter;
					break;
				case 3:
					adapter = new EventsAdapter(this, holidayList, theme);
					EventList.Adapter = adapter;
					break;
				case 4:
					adapter = new EventsAdapter(this, birthdayList, theme);
					EventList.Adapter = adapter;
					break;
				case 5:
					adapter = new EventsAdapter(this, assignmentList, theme);
					EventList.Adapter = adapter;
					break;
				default:
					adapter = new EventsAdapter(this, myList, theme);
					EventList.Adapter = adapter;
					break;
			}


		}

		// Method when search bar is open
		// Reference: http://blog.lovelyhq.com/implementing-a-live-list-search-in-android-action-bar/
		private void openSearchBar(string queryText)
		{


			// Set custom view on action bar, to support the search bar.
			Android.Support.V7.App.ActionBar actionBar = this.SupportActionBar;
			SupportActionBar.SetDisplayShowCustomEnabled(true);
			SupportActionBar.SetCustomView(Resource.Layout.search_bar);

			// Do the search of user's input
			searchBar = (EditText)actionBar.CustomView.FindViewById(Resource.Id.searchBarField);
			searchBar.AddTextChangedListener(
				new MyTextWatcher(this, theme));

			searchBar.Text = queryText;
			searchBar.RequestFocus();

			// Change search icon
			mSearchAction.SetIcon(mIconCloseSearch);
			mSearchOpened = true;

		}

		public class MyTextWatcher : Java.Lang.Object, ITextWatcher
		{
			private MainActivity _main;
			private int _theme;


			public MyTextWatcher(MainActivity a, int theme)
			{
				this._main = a;
				this._theme = theme;
			}
			public void AfterTextChanged(IEditable s)
			{
				_main.mSearchQuery = _main.searchBar.Text;
				_main.searchList.Clear();

				for (int i = 0; i < _main.myList.Count; i++)
				{
					string[] tempArray = _main.myList.ElementAt(i);
					if (tempArray[1].ToLower().Contains(_main.mSearchQuery.ToLower()))
					{
						_main.searchList.Add(tempArray);
					}
				}

				EventsAdapter adapter = new EventsAdapter(_main, _main.searchList, _theme);
				_main.EventList.Adapter = adapter;

			}
			public void BeforeTextChanged(Java.Lang.ICharSequence arg0, int start, int count, int after) { }
			public void OnTextChanged(Java.Lang.ICharSequence arg0, int start, int before, int count) { }
		}

		protected override void OnResume()
		{
			base.OnResume();

			if (activityCreated == true)
			{
				activityCreated = false;
			}
			else {
				Finish();
				StartActivity(Intent);
			}

		}

		/**
	     * Fragment that appears in the "content_frame", shows a planet
	     */
		internal class CustomFragment : Fragment
		{

			public MainActivity _main_temp;
			public CustomFragment fragment;


			DBEventHelper dbEventHelper;

			public ListView EventList;
			ImageView imageview;
			public Context context;

			public const string ARG_SELECTED_NUMBER = "planet_number";

			public CustomFragment(int position, MainActivity am)
			{
				_main_temp = am;
				if (_main_temp != null)
				{
					fragment = new CustomFragment();
					//_main_temp.fragment = this.fragment;
					Bundle args = new Bundle();
					args.PutInt(CustomFragment.ARG_SELECTED_NUMBER, position);
					fragment.Arguments = args;
				}
			}

			public CustomFragment()
			{
				// Empty constructor required for fragment subclasses
			}

			public static Fragment NewInstance(int position)
			{

				CustomFragment fragment = new CustomFragment();

				Bundle args = new Bundle();
				args.PutInt(CustomFragment.ARG_SELECTED_NUMBER, position);
				fragment.Arguments = args;
				return fragment;
			}





			public override View OnCreateView(LayoutInflater inflater, ViewGroup container,
											   Bundle savedInstanceState)
			{
				// Filter myList array to only events that are upcoming
				dbEventHelper = new DBEventHelper(Activity);
				var rootView =
					inflater.Inflate(Resource.Layout.fragment_drawer, container, false);
				var i = this.Arguments.GetInt(ARG_SELECTED_NUMBER);
				var title = this.Resources.GetStringArray(Resource.Array.event_array)[i];
				this.Activity.Title = title;

				return rootView;
			}
		}
	}
}

