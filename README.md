# How to test the different theme? #
Go to setting page and then change the theme style
then restart the app or simply go back to Main screen, you will see different theme immediately.

# How to test the different language? #
Changing language is exactly same process with changing the app theme, please follow process of changing the theme.
Four different languages are supported now: English, Chinese, Vietnamese, Indonesian.

# How to test the alarm music function? #
In add_activity
1.Disable the datavalidation() by change the equal to unequal in the formula when checking the result.![1.png](https://bitbucket.org/repo/XkgGrB/images/77114866-1.png)
2.disable the original alarmManage wcich named am1, make a new am1:
am1.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),alarmRepeatTime, pi);
3. run and set a time whatever it is.

# How to test if the alarm function is working properly? #
In add_activity
1.give a new value to alarmStartTime and alarmRepeatTime;************************
2.alarmStartTime = selectedTime.getTimeInMillis()
  alarmRepeatTime; = "10000" //which is 10 seconds
3. run and select a time after few minutes, then alarm should repeatitively remind u every 10 seconds. It will stop by the time selected.
I have specified this in the AddActivity.

# About Dialog use#
Please use the CustomeDialog instead of AlertDialog, because CustomDialog is adjustbale with our different theme

```
#!C#

CustomDialog.Builder builder = new CustomDialog.Builder(this);
builder.setMessage(GetString(Resource.String.freq_long_text));
builder.setTitle("eTime Reminder");
builder.setPositiveButton(GetString(Resource.String.yes_text), delegate
  {
    if (builder.ifChecked() == true)
  {
    //do something when checkbox in the alert is checked
  }
});

builder.setNegativeButton(GetString(Resource.String.cancel_text), delegate
  {
    //do something
});
builder.setCheckBox(GetString(Resource.String.remember_text));
builder.create(false, theme).Show();
```


# How to delete event? #
```
#!java
DBEventHelper dbEventHelper = new DBEventHelper(this); //create helper for event
dbEventHelper.deleteEvent(wventID);

```

# How to get event in ListView?#
1. make sure you find a way to be able to identify each row in ListView by its AlarmID even it's a past event.
2. once you embedded each AlarmID with each row which indicates an event, the you will be able to modify the event in whatever way you like e.g. delete, edit, view etc. 

# How to update an event with any column u want? #
 
```
#!java
DBEventHelper dbEventHelper = new DBEventHelper(this); //create helper for event
EventClass event = new EventClass();//change the event to history
event = dbEventHelper.getEvent(String.valueOf(AlarmID)); //initiate all data in event
event.setStatus(false); //this is example for modify the status of event
dbEventHelper.updateEvent(event);
```